from typing import Iterable, List, Tuple, Any
import platform, os

class Helpers:
    @staticmethod
    def clear() -> None:
        os.system("cls") if platform.system() == "Windows" else os.system("clear")

class Usuario:
    def __init__(self, id_usuario: int,  nombre: str, apellido: str, apellido2: str) -> None:
        self.id = id_usuario
        self.nombre = nombre
        self.apellido = apellido
        self.apellido2 = apellido2
        self.usuario: str
        self.contrasenia: str
        self.vuelo: List[Vuelo] = []

    def crear_cuenta(self, usuario: str, contrasenia: str) -> None:
        self.usuario, self.contrasenia = usuario, contrasenia

    def add_vuelo(self, vuelo: Any) -> None:
        self.vuelo.append(vuelo)

    def __str__(self):
        return f"nombre: {self.nombre} - apellidos: {self.apellido} {self.apellido2}"

class Registro:
    def __init__(self):
        self.usuarios: List[Usuario] = []

    def guardar_datos(self, usuario: Usuario) -> None:
        self.usuarios.append(usuario)

    def validar_usuario(self, usuario: str, contrasena: str) -> Tuple[Usuario, bool]:
        for usr in self.usuarios:
            if usr.usuario == usuario and usr.contrasenia == contrasena:
                return usr, True
        return None, False

    def __iter__(self) -> Iterable[Usuario]:
        return iter(self.usuarios)

class Avion:
    def __init__(self, nombre: str, capacidad: str) -> None:
        self.nombre = nombre
        self.capacidad = capacidad

    def __str__(self) -> str:
        return f"{self.nombre} - {self.capacidad}"

class Vuelo:
    def __init__(self, id_v: int, h_salida: int, h_llegada: str, avion: Avion) -> None:
        self.id_vuelo = id_v
        self.hora_salida = h_salida
        self.hora_llegada = h_llegada
        self.avion = avion
        self.aeropuerto_origen: Aeropuerto
        self.aeropuerto_destino: Aeropuerto
        self.usuarios: List[Usuario] = []

    def agregar_ruta(self, aeropuerto_origen: Any, aeropuerto_destino: Any) -> None:
        self.aeropuerto_origen, self.aeropuerto_destino = aeropuerto_origen, aeropuerto_destino

    def agregar_pasajero(self, usuario: Usuario) -> None:
        self.usuarios.append(usuario)

    def __str__(self) -> str:
        return f"id: {self.id_vuelo}, salida:{self.hora_salida} : llegada:{self.hora_llegada}, avion:{self.avion}"

    @staticmethod
    def validar_horario(hora: int) -> bool: return hora >= 0 and hora <= 24

class Aeropuerto:
    def __init__(self, nombre: str, ubicacion: str) -> None:
        self.nombre = nombre
        self.ubucacion = ubicacion
        self.vuelos: List[Vuelo] = []

    def agregar_vuelo(self, vuelo) -> None:
        self.vuelos.append(vuelo)

    def __str__(self) -> str:
        return f"{self.nombre} - {self.ubucacion}"

class SistemaAeropuerto:
    def __init__(self) -> None:
        self.aeopuertos: List[Aeropuerto] = []
        self.aviones: List[Avion] = []
        self.vuelos_generales: List[Vuelo] = []
        self.registo = Registro()
        self.usr_session: Usuario

    def agregar_aviones(self, a: Avion) -> None:
        self.aviones.append(a)

    def agregar_aeropuertos(self, a: Aeropuerto) -> None:
        self.aeopuertos.append(a)

    def menu(self) -> None:
        while True:
            Helpers.clear()
            print("""
            LOGIN Y REGISTRO DE USUARIO
            1.INICIAR SESION
            2.REGISTRARSE
            3.Salir""")
            opcion = int(input("Digite su opcion:"))
            if opcion == 1:
                self.login()
            elif opcion == 2:
                self.registrarse()
            elif opcion == 3:
                print("QUE TENGA UN BUEN DIA")
                break
            else:
                print("EL VALOR INGRESADO NO EXISTE")
            input("[enter continuar...]")

    def login(self) -> None:
        while True:
            usuario = input("Digite su nombre de usuario: ")
            usuario_contrasena = input("Digite su contraseña: ")

            if (usr_ok := self.registo.validar_usuario(usuario, usuario_contrasena))[1]:
                self.usr_session, _ = usr_ok
                self.menu_usuario()
                break
            else:
                print("EL NOMBRE DE USUARIO O LA CONTRASEÑA NO SE ENCUENTRA O ES INCORRECTO")

    def registrarse(self) -> None:
        id = int(input("id:"))
        nombre = input("Nombre:")
        apellido1 = input("Primer apellido:")
        apellido2 = input("Segundo apellido:")
        username = input("Nombre de usuario:")
        contrasena = input("Contraseña:")

        nuevo_usuario = Usuario(id, nombre, apellido1, apellido2)
        nuevo_usuario.crear_cuenta(username, contrasena)

        self.registo.guardar_datos(nuevo_usuario)
        self.show_registros()

    def menu_usuario(self) -> None:
        while True:
            Helpers.clear()
            print(f"""
            ----------------Bienvenido al menu sistema de vuelos--------------------
            Bienvenido: {self.usr_session.nombre} {self.usr_session.apellido} {self.usr_session.apellido2}
            1.Crear Vuelo
            2.Eliminar Vuelo
            3.Primer Vuelo en Aterrizar
            4.Vuelo con Aeropuerto Especifico
            5.Cantidad de Vuelos en el Aires
            6.Iniciar Sesion con otra Cuenta""")

            op = int(input("Digite su opcion:"))

            if op == 1:
                self.crear_vuelo()
            elif op == 2:
                self.eliminar_vuelo()
            elif op == 3:
                self.primer_vuelo()
            elif op == 4:
                self.vuelo_por_aeropuerto()
            elif op == 5:
                print(len(self.vuelos_generales))
            elif op == 6:
                return
            else:
                print("EL VALOR INGRESADO ES INCORRECTO")
            input("{enter}...")

    def primer_vuelo(self) -> None:
        p_vuelo = self.vuelos_generales[0]
        for v in self.vuelos_generales:
            if v.hora_llegada > p_vuelo.hora_llegada:
                p_vuelo = v
        print(f"El primer vuelo en llegar es:\n {p_vuelo}")

    def crear_vuelo(self) -> None:
        id_vuelo = int(input("Digite el Identificador de Vuelo:"))
        while not Vuelo.validar_horario(hora_salida := int(input("Digite la hora de Salida:"))):
            print("INGRESE CORRECTAMENTE LA HORA DE SALIDA")

        while not Vuelo.validar_horario(hora_llegada := int(input("Digite la Hora de Llegada:"))):
            print("INGRESE CORRECTAMENTE LA HORA DE LLEGADA")

        print("SALIDA")
        aeropuerto_salida = self.seleccionar_aeropuerto()
        print("LLEGADA")
        aeropuerto_llegada = self.seleccionar_aeropuerto()
        print("AVION")
        avion = self.seleccionar_avion()

        nuevo_vuelo = Vuelo(id_vuelo, hora_salida, hora_llegada, avion)
        nuevo_vuelo.agregar_ruta(aeropuerto_salida, aeropuerto_llegada)
        nuevo_vuelo.agregar_pasajero(self.usr_session)

        aeropuerto_salida.agregar_vuelo(nuevo_vuelo)

        self.usr_session.add_vuelo(nuevo_vuelo)
        self.vuelos_generales.append(nuevo_vuelo)

    def seleccionar_aeropuerto(self) -> Aeropuerto:
        self.show_aeropuertos()
        while (op := int(input("INGRESE UNA OPCION:"))) < 0 or op > len(self.aeopuertos):
            print("EL VALOR INGRESADO NO EXISTE O ES INCORRECTO")
        return self.aeopuertos[op - 1]

    def seleccionar_avion(self) -> Avion:
        self.show_aviones()
        while (op := int(input("INGRESE OPCION:"))) < 0 or op > len(self.aviones):
            print("EL VALOR INGRESADO NO EXISTE O ES INCORRECTO")
        return self.aviones[op - 1]

    def show_aeropuertos(self) -> None:
        for i, ae in enumerate(self.aeopuertos, 1):
            print(f"{i} - {ae}")

    def show_aviones(self) -> None:
        for i, av in enumerate(self.aviones, 1):
            print(f"{i} - {av}")

    def eliminar_vuelo(self) -> None:
        print("ELIMINA TUS VUELOS")
        if len(self.usr_session.vuelo) == 0:
            print("No tienes vuelos!!!"); return
        for i, v in enumerate(self.usr_session.vuelo, 1):
            print(f"{i} - {v}")
        while (op := int(input("INGRESE UNA OPCION:"))) < 0 or op > len(self.usr_session.vuelo):
            print("EL VALOR INGRESADO NO EXISTE O ES INCORRECTO")

        vuelo_eliminado = self.usr_session.vuelo.pop(op - 1)
        vuelo_eliminado.usuarios.remove(self.usr_session)
        vuelo_eliminado.aeropuerto_destino.vuelos.remove(vuelo_eliminado)
        self.vuelos_generales.remove(vuelo_eliminado)
        print(f"Vuelo eliminado {vuelo_eliminado}")

    def vuelo_por_aeropuerto(self) -> None:
        for a in self.aeopuertos:
            print(f"Aeropuerto: {a}\nVuelos:")
            for v in a.vuelos:
                print(v)

    def show_registros(self) -> None:
        for usuario in self.registo:
            print(usuario)

def main() -> None:
    aviones = [
        Avion("Airbus A320", "177 pasajeros"),
        Avion("Túpolev Tu-204", " 212 pasajeros"),
        Avion("Ilyushin Il-96", " 435 pasajeros"),
        Avion("Boeing 747-8", " 467 pasajeros")
    ]
    aeropuertos = [
        Aeropuerto("Juan Santamaría International", "Costa Rica"),
        Aeropuerto("Internacional Augusto C. Sandino (Managua)", "Nicaragua"),
        Aeropuerto("Internacional Hartsfield-Jackson", "Atlanta EE UU"),
        Aeropuerto("Aeropuerto de Rosario", "Argentina")
    ]

    sistema_aeropuerto = SistemaAeropuerto()

    for _aviones, _aeropuertos in zip(aviones, aeropuertos):
        sistema_aeropuerto.agregar_aviones(_aviones)
        sistema_aeropuerto.agregar_aeropuertos(_aeropuertos)

    sistema_aeropuerto.menu()

if __name__ == "__main__":
    main()
